function [signal] = staticArraySignal(wfm, awg_trap, memSamples)
    %STATICARRAYSIGNAL Summary of this function goes here
    %   Detailed explanation goes here

    if ~exist('memSamples', 'var')
        memSamples = awg_trap.memSamples;
    end
    samplerate = awg_trap.samplerate;
    t = (1:memSamples) / samplerate;

    signal = sum(wfm.amp' .* (sin(2 * pi * wfm.freq' * t + wfm.phase')), 1);


    numTweezers = length(wfm.freq);
    scale = 2^15 - 1; 
    ampMax = scale / sqrt(numTweezers);
    signal = ampMax * signal;

    if max(abs(signal)) > 2^15
        signal(signal > 2^15) = 2^15;
        signal(signal < -2^15) = -2^15;
        warnStruct = warning(); warning('on'); warning('Waveform goes above digital limit, signal will be clipped!'); warning(warnStruct);
    end

end
