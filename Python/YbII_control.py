from lib.AWG import *
import code
# import readline
import rlcompleter
import os
import argparse
from lib.waveform import *
import sys
from datetime import datetime, timedelta
from pathlib import Path
from memory_profiler import profile
import time

try:
    profile
except NameError:
    no_profile = lambda x: x
    profile = no_profile

def get_step():
    step = int32(0)
    spcm_dwGetParam_i32(awg.card, SPC_SEQMODE_STATUS, byref(step))
    return step.value

def trigger():
    global awg
    # step = get_step()
    awg.force_trigger()
    print("triggering...")
    # print("current step:", step)


def stop():
    global awg
    awg.stop()
    print("stopping awg output...")

def start():
    global awg
    if not awg.is_connected():
        sys.exit(0)
    awg.run()
    awg.force_trigger()
    print("starting awg output...")

def close():
    global awg
    awg.stop()
    awg.reset()
    awg.close()
    print("closing awg and quitting...")
    exit(0)

np.random.seed(0)
sampling_rate = int(614.4e6)

# array_H = Waveform(
#     60e6,
#     2e6,
#     40,
#     freq_res=10e3,
# )
# print(array_H.sample_len_min)
# array_V = Waveform()
# array_V.copy(array_H)

array_H = Waveform()
array_V = Waveform()
# array_H.from_file("data/array_80-4-10.npz")
# array_V.from_file("data/array_80-4-10.npz")
array_H.from_file("data/array_60-2-40.npz")
array_V.from_file("data/array_60-2-40.npz")

# array_H.phi = np.zeros(array_H.phi.shape[0])
sig_H = create_static_array(array_H, full=False)
sig_V = create_static_array(array_V, full=False)
sig = np.empty(len(sig_H)*2, dtype=sig_H.dtype)
sig[0::2] = sig_H
sig[1::2] = sig_V

sig0 = sig
# sig1 = sig_V
# print(sig1.shape, sig0.shape)
# AWG stuff

# condition for awg step sequence. 
# 0 for infinite loop until trigger
# 1 for finite loop, then goto nextstep afterwards.
condition = [SPCSEQ_ENDLOOPONTRIG, SPCSEQ_ENDLOOPALWAYS]
awg = AWG()
awg.open(id=0)  # change this to 0 for top AWG, 1 for bot AWG
awg.set_sampling_rate(sampling_rate)
awg.toggle_channel([0, 3], amplitude=2500)
awg.set_trigger(EXT0=SPC_TM_POS)
awg.set_sequence_mode(2)  # partition AWG memory into 2 segments
awg.write_segment(sig0, segment=0)
# awg.write_segment(sig1, segment=1)
awg.configure_step(step=0, segment=0, nextstep=0, loop=1, condition=condition[0])
# awg.configure_step(step=1, segment=1, nextstep=0, loop=1, condition=condition[0])
start()

# console
vars = globals()
vars.update(locals())
# readline.set_completer(rlcompleter.Completer(vars).complete)
# readline.parse_and_bind("tab: complete")
code.InteractiveConsole(vars).interact("")
