#include "image_process.h"
#include <filesystem>
#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/imgcodecs.hpp>
#include <iostream>
#include "external/npy.hpp"

// void TwzrImg::loadImage(
//     std::vector<uint8_t> rawImg,
//     uint width,
//     uint height
// ) {
//     this->img = cv::Mat(height, width, CV_8U, rawImg.data());
// }

// void TwzrImg::loadImage(std::string filename) {
//     this->img = cv::imread(filename, cv::IMREAD_GRAYSCALE);
// }

cv::Mat ImageProcessing::cvFromRaw(
    uint8_t* buffer,
    uint width,
    uint height
) {
    return cv::Mat(height, width, CV_8U, buffer);
}

std::vector<std::vector<uint>> ImageProcessing::getPeaks(
    const cv::Mat img,
    double threshold,
    std::string saveViewPath
) {
    double maxVal;
    cv::minMaxIdx(img, NULL, &maxVal, NULL, NULL);
    double threshVal = maxVal * threshold;

    cv::Mat proc;
    cv::GaussianBlur(img, proc, cv::Size(0,0), 3, 3);
    if (threshold > 0) {
        cv::threshold(proc, proc, threshVal, 255, cv::THRESH_BINARY);
    } else {
        cv::threshold(proc, proc, cv::THRESH_TRIANGLE, 255, cv::THRESH_BINARY);
    }
    cv::Mat labels, stats, centroids;
    cv::connectedComponentsWithStats(
        proc,
        labels,
        stats,
        centroids,
        4,
        CV_32S,
        cv::CCL_SAUF
    );
    std::vector<std::vector<uint>> peakLocs;
    std::filesystem::path savePath(saveViewPath);
    bool saveView = std::filesystem::exists(savePath) ? true : false;
    cv::Mat imgCopy;
    if (saveView) { img.copyTo(imgCopy); }
    for (auto i = 1; i < centroids.rows; i++) {
        cv::Point2d p(centroids.row(i));
        std::vector<uint> row;
        row.push_back(p.x);
        row.push_back(p.y);
        peakLocs.push_back(row);
        if (saveView) { cv::circle(imgCopy, p, 3, 255); }
    }
    if (saveView) { 
        // savePath = savePath / "figs";
        // if (!std::filesystem::exists(savePath)) {
        //     std::filesystem::create_directories(savePath);
        // }
        cv::imwrite(savePath / "peaks.png", imgCopy); 
    } else { 
        std::cout << savePath << " invalid path, "
            << "write to file in ImageProcessing::getPeaks "
            << "failed\n";
    }
    return peakLocs;
}

std::vector<cv::Mat> ImageProcessing::getSubImgs(
    const cv::Mat img,
    std::vector<std::vector<uint>> peaks,
    std::string saveViewPath
) {
    std::filesystem::path savePath(saveViewPath);
    bool saveView = std::filesystem::exists(savePath) ? true : false;
    if (saveView) {
        // savePath = savePath / "figs";
        // if (!std::filesystem::exists(savePath)) {
        //     std::filesystem::create_directories(savePath);
        // }
    } else {
        std::cout << savePath << " invalid path, "
            << "write to file in ImageProcessing::getSubImgs "
            << "failed\n";
    }
    std::vector<cv::Mat> subImages;
    auto w = 80;
    auto h = 50;
    for (int i = 0; auto peak : peaks) {
        auto x = peak[0] - w / 2;
        auto y = peak[1] - h / 2;
        cv::Rect roi(x, y, w, h);
        cv::Mat cropimg = img(roi);
        subImages.push_back(cropimg);
        if (saveView) {
            std::string filename = "crop_" + std::to_string(i) + ".png";
            cv::imwrite(savePath / filename, cropimg); 
        }
        ++i;
    }
    return subImages;
}

std::vector<double> ImageProcessing::getPowers(const std::vector<cv::Mat> imgs) {
    std::vector<double> powers;
    for (auto img : imgs) {
        powers.push_back(cv::sum(img)[0]);
    }
    return powers;
}
