#pragma once
#include "waveform.h"
#include "image_process.h"
#include "devices/AWG.h"
#include "devices/basler.h"

namespace Uniformization {

    struct Params {
        std::string probeScanPath;
        double polarizability;
        double meanDepth;
        double stepSize;
        double errorThreshold;
        int maxLoop;
        int numImgingAvg;
        int numTweezer;
    };

    Eigen::ArrayXd getWeightedMask(
        Eigen::Ref<Eigen::ArrayXd> detProbe,
        Eigen::Ref<Eigen::ArrayXd> initPower,
        double polarizability,
        double meanDepth
    );

    void reloadAWG(
        AWG& awg,
        ArrayWaveform& wfm
    );

    cv::Mat grabMulti(
        BaslerCam& basler,
        int numGrab
    );

    void saveCSV(
        std::string filename,
        std::vector<double> data
    );

    void run(
        AWG& awg,
        BaslerCam& basler,
        ArrayWaveform& waveform,
        const Params& config
    );

};