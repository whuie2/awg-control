#pragma once
#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/core.hpp>
#include <opencv4/opencv2/highgui.hpp>

namespace ImageProcessing {

    cv::Mat cvFromRaw(
        uint8_t* buffer,
        uint width,
        uint height
    );

    std::vector<std::vector<uint>> getPeaks(
        const cv::Mat img,
        double threshold=-1,
        std::string saveViewPath=""
    );

    std::vector<cv::Mat> getSubImgs(
        const cv::Mat img,
        std::vector<std::vector<uint>> peaks,
        std::string saveViewPath=""
    );
    
    std::vector<double> getPowers(const std::vector<cv::Mat> imgs);

};