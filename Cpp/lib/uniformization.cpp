#include "uniformization.h"
#include "external/npy.hpp"
#include <filesystem>
#include <ctime>

Eigen::ArrayXd Uniformization::getWeightedMask(
    Eigen::Ref<Eigen::ArrayXd> detProbe,
    Eigen::Ref<Eigen::ArrayXd> initPower,
    double polarizability,
    double meanDepth
) {
    auto depths = meanDepth + (detProbe - detProbe.mean()) / polarizability;
    Eigen::ArrayXd mask = depths / depths.mean() 
        * initPower.mean() / initPower;
    return mask;
}

void Uniformization::reloadAWG(AWG& awg, ArrayWaveform& wfm) {
    auto wfmData = wfm.getStaticWaveform();
    awg.cardStop();
    awg.writeSeqModeSegment(
        0,
        wfmData.first,
        wfmData.second
    );
    awg.cardRun();
    awg.forceTrigger();
}

cv::Mat Uniformization::grabMulti(
    BaslerCam& basler,
    int numGrab
) {
    if (!basler.isGrabbing()) { basler.startGrabbing(); }
    auto img = basler.getImage();
    Eigen::Map<Eigen::Array<uint8_t, -1, 1>> imgEigen(img.data(), img.size());
    auto width = basler.getWidth();
    auto height = basler.getHeight();
    for (int i = 0; i < numGrab - 1; i++) {
        auto newImg = basler.getImage();
        imgEigen += Eigen::Map<Eigen::Array<uint8_t, -1, 1>> (newImg.data(), newImg.size());
    }
    imgEigen /= numGrab;
    cv::Mat imgCV = ImageProcessing::cvFromRaw(img.data(), width, height);
    return imgCV;
}

void Uniformization::saveCSV(
    std::string filename,
    std::vector<double> data
) {
    std::ofstream savefile(filename);
    if (savefile.is_open()) {
        for (auto d : data) {
            savefile << d << ",";
        }
        savefile.close();
    } else {
        std::cout << filename << " invalid, "
            << "saving to file failed" << std::endl;
    }
}

void Uniformization::run(
    AWG& awg,
    BaslerCam& basler,
    ArrayWaveform& waveform,
    const Uniformization::Params& config
) {
    std::filesystem::path probePath(config.probeScanPath);
    if (!probePath.has_extension()) {
        probePath /= "probe-det.npy";
    }
    if (!std::filesystem::exists(probePath)) {
        std::cout << config.probeScanPath << " invalid, " 
            << "returning" << std::endl;
        return;
    }
    auto savePath = probePath.parent_path() / "uniformization";
    if (!std::filesystem::exists(savePath)) {
        std::filesystem::create_directory(savePath);
    }

    std::filesystem::path figSavePath = savePath / "figs";
    if (!std::filesystem::exists(figSavePath)) {
        std::filesystem::create_directory(figSavePath);
    }
    std::filesystem::path dataSavePath = savePath;
    auto wfmSavePath = dataSavePath / "initWfmParam.csv";
    waveform.saveParam(wfmSavePath);

    auto detProbe = npy::read_npy<double>(probePath).data;
    std::reverse(detProbe.begin(), detProbe.end());

    auto twzrImageInit = grabMulti(basler, config.numImgingAvg);
    auto peaks = ImageProcessing::getPeaks(twzrImageInit, -1, figSavePath);
    auto twzrCropsInit = ImageProcessing::getSubImgs(
        twzrImageInit,
        peaks,
        figSavePath
    );
    if (twzrCropsInit.size() != config.numTweezer) {
        std::cout
            << "specified " << config.numTweezer << " tweezers, "
            << twzrCropsInit.size() << " found, "
            << "returning..." << std::endl;
        return;
    }
    auto initPower = ImageProcessing::getPowers(twzrCropsInit);
    auto weightedMask = getWeightedMask(
        Eigen::Map<Eigen::ArrayXd> (detProbe.data(), detProbe.size()),
        Eigen::Map<Eigen::ArrayXd> (initPower.data(), initPower.size()),
        config.polarizability,
        config.meanDepth
    );

    std::vector<double> powerErrTotal;
    std::vector<double> finalPower = initPower;
    for (int i = 0; i < config.maxLoop; i++) {
        std::cout << "\r" << "running uniformization "
            << "loop " << i+1 << "/" << config.maxLoop << ", ";
        auto twzrImage = grabMulti(basler, config.numImgingAvg);
        auto twzrCrops = ImageProcessing::getSubImgs(
            twzrImage,
            peaks
        );
        auto powers = ImageProcessing::getPowers(twzrCrops);
        Eigen::Map<Eigen::ArrayXd> powersEigen(powers.data(), powers.size());
        auto powerErrs = (
            weightedMask * powersEigen 
            - (weightedMask * powersEigen).mean()
        ) / (weightedMask * powersEigen).mean();
        std::cout << "mean abs error: " << std::setprecision(4) << std::fixed
            << powerErrs.abs().mean() << std::flush;
        if (powerErrs.abs().mean() <= config.errorThreshold) { break; }

        auto amps = waveform.getAmplitude();
        Eigen::Map<Eigen::ArrayXd> ampsEigen(amps.data(), amps.size());
        ampsEigen -= config.stepSize * powerErrs;
        waveform.setAmplitude(amps);
        reloadAWG(awg, waveform);

        powerErrTotal.push_back(powerErrs.abs().mean());
        if (i == config.maxLoop - 1) {
            finalPower = powers;
            std::cout << std::endl;
        }
    }
    auto finalWfmSavePath = dataSavePath / "finalWfmParam.csv";
    waveform.saveParam(finalWfmSavePath);
    
    auto initPowerFile = dataSavePath / "initial_power.csv";
    auto finalPowerFile = dataSavePath / "final_power.csv";
    auto errorFile = dataSavePath / "errors.csv";
    saveCSV(initPowerFile, initPower);
    saveCSV(finalPowerFile, finalPower);
    saveCSV(errorFile, powerErrTotal);
}
