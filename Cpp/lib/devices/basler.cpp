#include "basler.h"

BaslerCam::BaslerCam() {
    try {
        Pylon::PylonInitialize();
    } catch (const Pylon::GenericException& e) {
        std::cerr << "exception at Pylon initialization"
            << e.GetDescription() << std::endl;
    }
}

BaslerCam::~BaslerCam() {
    this->camera.StopGrabbing();
    Pylon::PylonTerminate();
}

void BaslerCam::open(int index) {
    Pylon::CTlFactory& tlFactory = Pylon::CTlFactory::GetInstance();
    Pylon::DeviceInfoList_t devices;
    if (tlFactory.EnumerateDevices(devices) == 0) {
        throw std::runtime_error("no basler camera detected");
    }
    if (devices.size() <= index) {
        std::cerr << devices.size() << " camera detected" << std::endl;
        throw std::runtime_error("invalid camera index");
    }
    this->camera.Attach(tlFactory.CreateDevice(devices[index]));
    this->camera.Open();
}

void BaslerCam::printDeviceInfo() {
  auto devInfo = this->camera.GetDeviceInfo();
  std::cout << "Printing cam info:" << "\n"
        << "Address: " << devInfo.GetAddress() << "\n"
        << "Device class: " << devInfo.GetDeviceClass() << "\n"
        << "Device ID: " << devInfo.GetDeviceID() << "\n"
        << "Readable name: " << devInfo.GetFriendlyName() << "\n"
        << "Full name: " << devInfo.GetFullName() << "\n"
        << "Model name: " << devInfo.GetModelName() << "\n"
        << "Serial port ID: " << devInfo.GetPortID() << "\n"
        << "Serial number: " << devInfo.GetSerialNumber() << "\n"
        << std::endl;
}

void BaslerCam::setExposure(double tauExp) {
    this->camera.ExposureTime.SetValue(
        tauExp,
        Pylon::IntegerValueCorrection_Nearest
    );
}

double BaslerCam::getExposure() { return this->camera.ExposureTime(); }

void BaslerCam::setFrameRate(uint fr) {
    this->camera.AcquisitionFrameRateEnable();
    this->camera.AcquisitionFrameRate.SetValue(
        fr,
        Pylon::IntegerValueCorrection_Nearest
    );
}

void BaslerCam::setFrameRateMax() {
    this->camera.AcquisitionFrameRateEnable();
    this->camera.AcquisitionFrameRate.SetToMaximum();
}

void BaslerCam::setGain(double gain) {
    this->camera.Gain.SetValue(gain);
}

double BaslerCam::getGain() { return this->camera.Gain(); }

void BaslerCam::setROI(
    uint offsetX,
    uint offsetY,
    uint width,
    uint height
) {
    this->camera.OffsetX.SetValue(offsetX);
    this->camera.OffsetY.SetValue(offsetY);
    this->camera.Height.SetValue(height);
    this->camera.Width.SetValue(width);
}

uint BaslerCam::getOffsetX() { return this->camera.OffsetX(); }

uint BaslerCam::getOffsetY() { return this->camera.OffsetY(); }

uint BaslerCam::getWidth() { return this->camera.Width(); }

uint BaslerCam::getHeight() { return this->camera.Height(); }

void BaslerCam::setAcquisitionMode(std::string mode) {
    this->camera.AcquisitionMode.FromString("Continuous");
}

void BaslerCam::printAvailAcqModes() {
    Pylon::StringList_t modes;
    this->camera.AcquisitionMode.GetSettableValues(modes);
    for (auto m : modes) {
        std::cout << m << "\n";
    }
}

void BaslerCam::startGrabbing() {
    this->camera.StartGrabbing(Pylon::GrabStrategy_LatestImageOnly);
}

void BaslerCam::stopGrabbing() { this->camera.StopGrabbing(); }

bool BaslerCam::isGrabbing() { return this->camera.IsGrabbing(); }

std::vector<uint8_t> BaslerCam::getImage() {
    Pylon::CGrabResultPtr pGrabResult;
    this->camera.RetrieveResult(5000, pGrabResult, Pylon::TimeoutHandling_ThrowException);
    if (pGrabResult->GrabSucceeded()) {
        uint8_t* pImage = (uint8_t*) pGrabResult->GetBuffer();
        return std::vector<uint8_t> (pImage, pImage + pGrabResult->GetWidth() * pGrabResult->GetHeight());
    }
    return std::vector<uint8_t>(); 
}
