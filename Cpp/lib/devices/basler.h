#pragma once
#include <pylon/PylonIncludes.h>
#include <pylon/BaslerUniversalInstantCamera.h>
#include <iostream>
#include <string>
#include <vector>

class BaslerCam {
    private:
    Pylon::CBaslerUniversalInstantCamera camera;

    public:

    BaslerCam();
    ~BaslerCam();
    void open(int index);
    void printDeviceInfo();

    void setExposure(double tauExp);
    double getExposure();

    void setFrameRate(uint fr);
    void setFrameRateMax();

    void setGain(double gain);
    double getGain();

    void setROI(uint offsetX, uint offsetY, uint width, uint height);
    uint getOffsetX();
    uint getOffsetY();
    uint getWidth();
    uint getHeight();

    void setAcquisitionMode(std::string mode);
    void printAvailAcqModes();

    void startGrabbing();
    void stopGrabbing();
    bool isGrabbing();

    std::vector<uint8_t> getImage();

};