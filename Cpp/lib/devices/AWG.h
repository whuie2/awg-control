#pragma once
#include "driver_header/dlltyp.h"
#include "driver_header/regs.h"
#include "driver_header/spcerr.h"
#include "driver_header/spcm_drv.h"
#include <string>
#include <set>
#include <map>
#include <vector>

/**
 * @brief Class for handling AWG M4i.6622-x8.
 * Manual can be found at
 * https://spectrum-instrumentation.com/products/details/M4i6622-x8.php
 */
class AWG {

    // collection of card status for printouts
    const std::map<int, std::string> STATUS_NAMES = {
        {M2STAT_CARD_TRIGGER,	 "card received trigger"},
        {M2STAT_CARD_READY,		 "card ready"},
        {M2STAT_DATA_BLOCKREADY, "data block ready"},
        {M2STAT_DATA_END,		 "data transfer ended"},
        {M2STAT_DATA_OVERRUN,	 "data transfer overrun"},
        {M2STAT_DATA_ERROR,		 "data transfer error"},
    };


private:
    // ---------------------card information---------------------
    drv_handle	pCardHandle;    // card handle for lib calls
    int32	    cardIdx;        // card index
    int32	    serialNumber;   // card serial number
    int64	    instMemSize;    // card installed memory size
    int32	    bytesPerSample; // available number of bytes per sample
    int64	    maxSampleRate;  // maximum sampling rate

    // ---------------------setting information---------------------
    std::set<int> activeChannels;

    /**
     * @brief throw error if detected.
     */
    void checkError();

public:
    /**
     * @brief exception overload to retrieve error msg from AWG
     */
    class CardException : std::exception {
        private:
        char* exceptionMsg;
        public:
        CardException(char* msg) : exceptionMsg(msg) {}
        const char* what() const throw() { return exceptionMsg; }
    };
    /**
     * @brief defines analog output state when idling
     */
    enum class CHANNEL_STOPLVL : int32 {
        ZERO	 = SPCM_STOPLVL_ZERO,     ///< 0 mV
        LOW		 = SPCM_STOPLVL_LOW,      ///< -2500 mV 
        HIGH	 = SPCM_STOPLVL_HIGH,     ///< +2500 mV 
        HOLDLAST = SPCM_STOPLVL_HOLDLAST, ///< last replaed sample amplitude 
    };
    /**
     * @brief defines card replay mode
     */
    enum class REPLAY_MODE : int32 {
        SINGLE		  = SPC_REP_STD_SINGLE,        ///< data generation from memory with programmed loop number of times after each triger event
        MULTI		  = SPC_REP_STD_MULTI,         ///< data generation from memory after multiple trigger events
        GATE		  = SPC_REP_STD_GATE,          ///< data generation from memory using external gate signal
        SINGLERESTART = SPC_REP_STD_SINGLERESTART, ///< data generation from memory once after each trigger event
        SEQUENCE	  = SPC_REP_STD_SEQUENCE,      ///< data generation from memory with a specially programmed memory sequence
        FIFO_SINGLE	  = SPC_REP_FIFO_SINGLE,       ///< continuous data generation after single trigger event, memory used as FIFO buffer
        FIFO_MULTI	  = SPC_REP_FIFO_MULTI,        ///< continuous data generation after multiple trigger events, memory used as FIFO buffer
        FIFO_GATE	  = SPC_REP_FIFO_GATE,         ///< continuous data geeneration using external gatesignal, memory used as FIFO buffer
    };
    enum class BUFFER_TYPE : int32 {
        DATA	  = SPCM_BUF_DATA,      ///< buffer is used to transfer standard sample data
        ABA		  = SPCM_BUF_ABA,       ///< buffer is used to readout ABA data
        TIMESTAMP = SPCM_BUF_TIMESTAMP, ///< buffer is used to readout timestep info
    };
    enum class TRANSFER_DIR : int32 {
        PCTOCARD  = SPCM_DIR_PCTOCARD,  ///< transfer from PC to card
        CARDTOPC  = SPCM_DIR_CARDTOPC,  ///< transfer from card to PC
        CARDTOGPU = SPCM_DIR_CARDTOGPU, ///< transfer from card to GPU
        GPUTOCARD = SPCM_DIR_GPUTOCARD, ///< transfer from GPU to card
    };
    enum class CLOCK_MODE : int32 {
        INTPLL	  = SPC_CM_INTPLL,      ///< enables internal programmable high precision Quartz 1 for sample clock generation
        EXTREFCLK = SPC_CM_EXTREFCLOCK, ///< enables internal PLL with external reference for sample clock generation
    };
    enum class TRIGGER_MASK : int32 {
        NONE	 = SPC_TMASK_NONE,     ///< no trigger source selected
        SOFTWARE = SPC_TMASK_SOFTWARE, ///< enable software trigger, not applicable to and mask
        EXT0	 = SPC_TMASK_EXT0,     ///< enable external trigger 0
        EXT1	 = SPC_TMASK_EXT1,     ///< enable external trigger 1
    };
    enum class TRIGGER_MODE : int32 {
        NONE	   = SPC_TM_NONE,               ///< no trigger source selected
        POS		   = SPC_TM_POS,                ///< trigger detection on positive edge
        NEG		   = SPC_TM_NEG,                ///< trigger detection on negative edge
        POS_REARM  = SPC_TM_POS | SPC_TM_REARM, ///< trigger detection on positive edge, trigger is armed when trigger level 1 is crossed
        NEG_REARM  = SPC_TM_NEG | SPC_TM_REARM, ///< trigger detection on negative edge, trigger is armed when trigger level 1 is crossed
        BOTH	   = SPC_TM_BOTH,               ///< trigger detection on positive and negative edges
        HIGH	   = SPC_TM_HIGH,               ///< trigger detection on HIGH levels (above trigger level 0)
        LOW		   = SPC_TM_LOW,                ///< trigger detection on LOW levels (below trigger level 0)
        WINENTER   = SPC_TM_WINENTER,           ///< window trigger for entering area between level 0 and level 1
        WINLEAVE   = SPC_TM_WINLEAVE,           ///< window trigger for leaving area between level 0 and 1
        INWIN	   = SPC_TM_INWIN,              ///< window trigger for signal inside window between level 0 and level 1
        OUTSIDEWIN = SPC_TM_OUTSIDEWIN,         ///< window trigger for signal outside window between level 0 and level 1
    };
    enum class SEQ_LOOPCONDITION : int64 {
        ALWAYS = SPCSEQ_ENDLOOPALWAYS, ///< unconditionally change to the next step, if defined loops for the current segment have been replayed
        ONTRIG = SPCSEQ_ENDLOOPONTRIG, ///< conditionally change to the next step on trigger detection, after a defined number of loops have been played
        END	   = SPCSEQ_END,           ///< marks current step to be the last step in the sequence, card is stopped afterwards
    };

    // functions
    AWG() noexcept;
    ~AWG();

    // ---------------------basic card setting---------------------
    /**
     * @brief opens a connection to AWG by index.
     * 
     * @param openIndex card index, 0 or 1
     */
    void  open(int openIndex);

    /**
     * @brief check if a connection to AWG exists.
     * 
     * @return true or false
     */
    bool  isOpen();

    /**
     * @brief close the connection to AWG.
     * 
     */
    void  close();

    /**
     * @brief resets the AWG card, equivalent to a power reset.
     *
     */
    void  reset();

    /**
     * @brief get card idx
     * 
     * @return index 
     */
    int   getCardIdx();

    /**
     * @brief get car serial number
     * 
     * @return int 
     */
    int   getSerialNumber();

    /**
     * @brief get installed memory size
     * 
     * @return memory size
     */
    int64 getInstMemSize();

    /**
     * @brief get sample byte length
     * 
     * @return int 
     */
    int   getBytesPerSample();

    /**
     * @brief get the maximum sampling rate
     * 
     * @return maximum sampling rate 
     */
    int64 getMaxSampleRate();

    /**
     * @brief set the card sampling rate
     * 
     * @param sampleRate value range: [0, getMaxSampleRate()]
     */
    void  setSampleRate(int64 sampleRate);

    /**
     * @brief get current sampling rate
     * 
     * @return sampling rate
     */
    int64 getSampleRate();

    // ---------------------channel output control---------------------
    /**
     * @brief set channels for next run, resets last set.
     * 
     * @param channels set of channel index (0-3)
     */
    void setActiveChannels(std::set<int> channels);

    /**
     * @brief set channel amplitude.
     * 
     * @param ch channel index
     * @param amp amplitude (mV)
     */
    void setChannelAmp(int ch, int amp);

    /**
     * @brief set channel stop level.
     * 
     * @param ch channel index
     * @param stopLvl options: ZERO,LOW,HIGH,HOLDLAST
     */
    void setChannelStopLvl(int ch, CHANNEL_STOPLVL stopLvl);

    /**
     * @brief toggle channel output.
     * 
     * @param ch channel index
     * @param enable true or false
     */
    void toggleChannelOutput(int ch, bool enable);

    /**
     * @brief channel macro set amplitude, stop level, and enables output.
     * 
     * @param ch channel index
     * @param amp amplitude (mV)
     * @param stopLvl enum CHANNEL_STOPLVL: ZERO, LOW, HIGH, HOLDLAST
     * @param enable enable/disable channel output
     */
    void setChannel(int ch, int amp, CHANNEL_STOPLVL stopLvl, bool enable);

    /**
     * @brief get number of channels available on card
     * 
     * @return number of available channels
     */
    int	 getChannelCount();

    /**
     * @brief get currently activated channels
     * 
     * @return set of activated channels
     */
    std::set<int> getChannelActivated();

    /**
     * @brief set channel 0(2) and channel 1(3) to differential output mode.
     *
     * @param chPair 0 for channel 0,1, 1 for channel 2,3
     * @param enable true or false
     */
    void setChannelDiffMode(int chPair, bool enable);

    /**
     * @brief set channel 0(2) and channel 1(3) to differential output mode.
     *
     * @param chPair 0 for channel 0,1, 1 for channel 2,3
     * @param enable true or false
     */
    void setChannelDoubleMode(int chPair, bool enable);

    // ---------------------card status control---------------------
    /**
     * @brief write current setup to card without starting hardware,
     * some settings may not be changed while card is running.
     * 
     */
    void writeSetup();

    /**
     * @brief start the card with all selected settings.
     */
    void cardRun();

    /**
     * @brief stop the card, no effect if not running.
     */
    void cardStop();

    /**
     * @brief wait until card finished current output run.
     * TODO: check if this blocks.
     */
    void waitReady();

    /**
     * @brief set a timeout time in ms, 0 to disable.
     *
     * @param time timeout (ms)
     */
    void setTimeOut(int time); // ms

    /**
     * @brief get current timeout time
     * 
     * @return timeout (ms)
     */
    int  getTimeOut();

    /**
     * @brief print current card status
     */
    void printCardStatus();

    // ---------------------card trigger control---------------------
    /**
     * @brief toggle the trigger detection.
     * 
     * @param enable
     */
    void   toggleTrigger(bool enable);

    /**
     * @brief send a software trigger equivalent to a physical trigger event.
     */
    void   forceTrigger();

    /**
     * @brief wait until trigger event.
     * TODO: check if this blocks.
     */
    void   waitTrigger();

    /**
     * @brief program the OR trigger mask.
     *
     * @param trigMasks 1 or more enum TRIGGER_MASK
     */
    void   setTrigMaskOr(std::initializer_list<TRIGGER_MASK> trigMasks);

    /**
     * @brief program the OR trigger mask.
     *
     * @param trigMasks vector of 1 or more enum TRIGGER_MASK
     */
    void   setTrigMaskOr(std::vector<TRIGGER_MASK> trigMasks);

    /**
     * @brief program the AND trigger mask.
     *
     * @param trigMasks 1 or more enum TRIGGER_MASK
     */
    void   setTrigMaskAnd(std::initializer_list<TRIGGER_MASK> trigMasks);

    /**
     * @brief program the AND trigger mask.
     *
     * @param trigMasks vector of 1 or more enum TRIGGER_MASK
     */
    void   setTrigMaskAnd(std::vector<TRIGGER_MASK> trigMasks);

    /**
     * @brief set the trigger mode of trigger channel.
     * 
     * @param trigChannel 0 or 1 (labeled EXT0 and EXT1 on board)
     * @param trigMode enum TRIGGER_MODE
     */
    void   setTrigMode(int trigChannel, TRIGGER_MODE trigMode);

    /**
     * @brief set the trigger input termination.
     *
     * @param term 0 (high impedance), 1 (50 Ohm).
     */
    void   setTrigTerm(int term);

    /**
     * @brief get the trigger input termination
     *
     * @return 0 (high impedance), 1 (50 Ohm)
     */
    int    getTrigTerm();

    /**
     * @brief set trigger channel's coupling to DC or AC.
     * 
     * @param channel trigger channel index
     * @param coupling 0 (DC), 1 (AC)
     */
    void   setTrigCoupling(int channel, int coupling);

    /**
     * @brief get trigger channel's coupling mode
     * 
     * @param channel trigger channel index
     * @return coupling mode, 0 (DC), 1 (AC)
     */
    int    getTrigCoupling(int channel);

    /**
     * @brief set trigger level.
     * 
     * @param channel trigger channel index
     * @param level -10000mV to +10000mV
     */
    void   setTrigLvl(int channel, int level);

    /**
     * @brief Get trigger level
     * 
     * @param channel trigger channel index
     * @return trigger level (mV)
     */
    int    getTrigLvl(int channel);

    /**
     * @brief set trigger channel rearm level.
     * 
     * @param channel trigger channel index
     * @param level -10000mV to +10000mV
     */
    void   setTrigRearmLvl(int channel, int level);

    /**
     * @brief get trigger channel rearm level
     * 
     * @param channel trigger channel index
     * @return rearm level (mV)
     */
    int64  getTrigRearmLvl(int channel);

    // ---------------------replay mode control---------------------
    //void  setReplayMode(REPLAY_MODE mode);
    //void  setMemSize(int64 sampleSize);
    //int64 getMemsize();
    //void  setLoop(int nLoop);
    //int64 getLoop();

    /**
     * @brief initialize sequence replay mode.
     * 
     * @param nSeg number of segments memory is divided into
     */
    void initReplayModeSeq(int nSeg);

    /**
     * @brief configure a step in sequence replay mode.
     * 
     * @param step step to configure
     * @param segment memory segment step is associated with
     * @param nextStep index of next step after end loop condition is met
     * @param nLoop number of loop the memory segment is replayed
     * @param condition end loop condition, enum SEQ_LOOPCONDITION
     */
    void setSeqModeStep(
        uint32 step,
        uint64 segment,
        uint64 nextStep,
        uint64 nLoop,
        SEQ_LOOPCONDITION condition
    );

    /**
     * @brief write buffered data into a memory segment in sequence replay mode.
     * 
     * @param segment memory segment to write to
     * @param pDataBuffer pointer to data memory, must be page aligned
     * @param size size of data buffer in number of samples
     */
    void writeSeqModeSegment(
        int segment,
        void* pDataBuffer, 
        int size
    );

    // ---------------------data transfer---------------------
    /**
     * @brief prepares the board for data transfer.
     * 
     * @param dataBuffer pointer to the data buffer
     * @param bufferLen length of data buffer, bytes
     * @param bufType buffer type, use only DATA for replay
     * @param dir direction of data transfer
     * @param notifySize number of bytes after which an event is sent
     * @param brdMemOffs offset for transfer in board memory
     */    
    void prepDataTransfer(
        void* dataBuffer,
        uint64 bufferLen,
        BUFFER_TYPE bufType = BUFFER_TYPE::DATA,
        TRANSFER_DIR dir = TRANSFER_DIR::PCTOCARD,
        uint32 notifySize = 0,
        uint64 brdMemOffs = 0
    );

    /**
     * @brief starts data transfer for a defined buffer.
     */
    void startDataTransfer();

    /**
     * @brief wait for data transfer to complete, blocks; 
     * timeout is considered.
     */
    void waitDataTransfer();

    /**
     * @brief stops a running data transfer.
     */
    void stopDataTransfer();

    // ---------------------clock control---------------------
    /**
     * @brief set the clock mode.
     * 
     * @param cm enum CLOCK_MODE
     */
    void  setClockMode(CLOCK_MODE cm);

    /**
     * @brief get clock mode
     * @return clock mode, check enum CLOCK_MODE
     */
    int32 getClockMode();

    /**
     * @brief if using EXTREFCLK mode, set the external clock frequency.
     * 
     * @param frequency Hz
     */
    void  setRefClkFreq(int64 frequency);

    /**
     * @brief enable/disable clock output.
     * 
     * @param enable true/false
     */
    void  setClockOut(bool enable);

    /**
     * @brief Read out frequency of internally synthesized clock.
     * 
     * @return frequency (Hz)
     */
    int64 getClockOutFreq();
};

