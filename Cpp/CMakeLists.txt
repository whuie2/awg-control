﻿# CMakeList.txt : CMake project for Cpp, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.20)
# respect <PACKAGE>_ROOT variables in
# Enable Hot Reload for MSVC compilers if supported.
if (POLICY CMP0141)
  cmake_policy(SET CMP0141 NEW)
  set(CMAKE_MSVC_DEBUG_INFORMATION_FORMAT "$<IF:$<AND:$<C_COMPILER_ID:MSVC>,$<CXX_COMPILER_ID:MSVC>>,$<$<CONFIG:Debug,RelWithDebInfo>:EditAndContinue>,$<$<CONFIG:Debug,RelWithDebInfo>:ProgramDatabase>>")
endif()

set(CMAKE_CXX_STANDARD 20)

# Non "standard" but common install prefixes
list(APPEND CMAKE_SYSTEM_PREFIX_PATH
#   /usr/X11R6
#   /usr/pkg
  /opt/
)

project ("main")

# add libraries
add_library(wfmLib "")
add_library(AWGLib "")
add_library(baslerLib "")
add_library(imageProcLib "")
add_library(uniformLib "")
add_subdirectory(lib)
include_directories(lib/driver_header)

set(LIBS
    wfmLib
    AWGLib
    baslerLib
    imageProcLib
    uniformLib
)

# executables
add_subdirectory(src)
