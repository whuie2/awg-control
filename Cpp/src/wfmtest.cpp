#include "../lib/waveform.h"
#include <iostream>
#include <filesystem>
#include "../lib/external/plf_nanotimer.h"
namespace fs = std::filesystem;

int main() {
    fs::path awgControl = "/home/simon/Documents/lab/code/awg-control";
    fs::path pyDataPath = awgControl / "Python/data";
    fs::path savePath = pyDataPath / "wfm.csv";
    auto nt = 20;
    ArrayWaveform wfm;
    wfm.setFreqTone(105e6, 1e6, nt);
    wfm.setDefaultParam();
    // Eigen::ArrayXd randPhase = Eigen::ArrayXd::Random(nt) * M_PI;
    // wfm.setPhase(randPhase);
    wfm.getStaticWaveform();
    wfm.saveWaveform(savePath);
    // plf::nanotimer timer;


    return 0;
}


