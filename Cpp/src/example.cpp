#include <iostream>
#include <set>
#include "../lib/waveform.h"
#include "../lib/devices/AWG.h"
// #include "../lib/devices/basler.h"

int main() {
    auto sr = 614.4e6;
    auto wfm = ArrayWaveform();
    // idealy, waveform param setting here are loaded from
    // another waveform via some sort of config file
    wfm.setSamplingRate(sr);
    wfm.setFreqResolution(1e3);
    // wfm.loadParam("testParam.csv");
    // wfm.saveParam(("testParam2.csv"));
    wfm.setFreqTone(
        105e6,
        1e6,
        5
    );
    wfm.setAmplitude(std::vector<double> (5, 1000));
    wfm.setPhase(std::vector<double> (5, 0));
    // wfm.printParam();
    // generate waveform
    auto wfmData = wfm.getStaticWaveform();
    // wfm.getStaticWaveform();
    // wfm.saveParam(("testParam.csv"));
    // wfm.saveWaveform("testWfm.csv");
    auto awg = AWG();
    awg.open(1); // open card at index 0
    // awg.printCardStatus();
    awg.setSampleRate(sr); // set card sampling rate
    awg.setActiveChannels(std::set<int> {0}); // set output channels
    awg.setChannel( // set channel output params
        0,
        2500,
        AWG::CHANNEL_STOPLVL::ZERO,
        true
    );
    awg.setTrigMode(
        0,
        AWG::TRIGGER_MODE::POS
    ); // set trigger channel 0 (EXT0) mode to be positve edge
    awg.setTrigMaskOr({
        AWG::TRIGGER_MASK::EXT0,
    //     AWG::TRIGGER_MASK::EXT1
    }); // set the trigger engine to be OR and enable both
        // channel EXT0 and/or EXT1
    // awg.initReplayModeSeq(2); // intializes card for sequence replay mode
    //                          // with two memory segments
    // awg.writeSeqModeSegment(
    //     0,
    //     wfmData.first,
    //     wfmData.second
    // );
    //    // a memory segment needs only be initialized with data
    //    // if it is configured as part of the step sequence below
    // awg.setSeqModeStep(
    //     0,
    //     0,
    //     1,
    //     1,
    //     AWG::SEQ_LOOPCONDITION::ONTRIG
    // );
    // awg.setSeqModeStep(
    //     1,
    //     1,
    //     0,
    //     1,
    //     AWG::SEQ_LOOPCONDITION::ONTRIG
    // );
    // awg.cardRun();
    // std::cout << "Enter: ";
    // std::string input;
    // std::getline(std::cin, input);
    // awg.cardStop();
    awg.close();
    
    return 0;
}