#include <iostream>
#include <set>
#include "../lib/waveform.h"
#include "../lib/devices/AWG.h"
#include <filesystem>
// #include "../lib/devices/basler.h"

int main() {
    auto sr = 614.4e6;
    auto wfm = ArrayWaveform();
    std::filesystem::path workingDir ("/home/coveylab/Documents/Control/awg-control/Cpp/");
    wfm.loadParam(workingDir / "data/test.csv");
    // wfm.setSamplingRate(sr);
    // wfm.setFreqResolution(1e3);
    // wfm.setFreqTone(
    //     105e6,
    //     1e6,
    //     5
    // );
    // wfm.setAmplitude(std::vector<double> (5, 1000));
    // wfm.setPhase(std::vector<double> (5, 0));
    // wfm.printParam();
    auto wfmData = wfm.getStaticWaveform();


    auto awg = AWG();
    awg.open(0); // open card at index 0
    awg.setSampleRate(sr); // set card sampling rate
    awg.setActiveChannels(std::set<int> {0}); // set output channels
    awg.setChannel( // set channel output params
        0,
        2500,
        AWG::CHANNEL_STOPLVL::ZERO,
        true
    );
    awg.setTrigMode(
        0,
        AWG::TRIGGER_MODE::POS
    ); // set trigger channel 0 (EXT0) mode to be positve edge
    awg.setTrigMaskOr({
        AWG::TRIGGER_MASK::EXT0,
    });
    awg.initReplayModeSeq(2);
    awg.writeSeqModeSegment(
        0,
        wfmData.first,
        wfmData.second
    );
    awg.writeSeqModeSegment(
        1,
        wfmData.first,
        wfmData.second
    );
    awg.setSeqModeStep(
        0,
        0,
        1,
        1,
        AWG::SEQ_LOOPCONDITION::ONTRIG
    );
    awg.setSeqModeStep(
        1,
        1,
        0,
        1,
        AWG::SEQ_LOOPCONDITION::ONTRIG
    );
    awg.cardRun();
    awg.forceTrigger();
    std::cout << "Enter: ";
    std::string input;
    std::getline(std::cin, input);
    awg.cardStop();
    awg.close();
    
    return 0;
}