#include "../lib/waveform.h"
#include "../lib/devices/AWG.h"
#include "../lib/devices/basler.h"
#include "../lib/image_process.h"
#include "../lib/uniformization.h"
#include "../lib/external/npy.hpp"
#include <filesystem>
#include <unistd.h>
#include <chrono>
#include <ctime>

#define TEST_ZERO 0
#define TEST_ONE 1

template <typename T>
void printVec(std::vector<T>& vec) {
    for (auto i : vec) {
        std::cout << i << ",";
    }
    std::cout << "\n";
}

Eigen::ArrayXd getWeightedMask(
    Eigen::Ref<Eigen::ArrayXd> detProbe,
    Eigen::Ref<Eigen::ArrayXd> initPower,
    double polarizability,
    double meanDepth
) {
    auto depths = meanDepth + (detProbe - detProbe.mean()) / polarizability;
    Eigen::ArrayXd mask = depths / depths.mean() 
        * initPower.mean() / initPower;
    return mask;
}

int main() {
    // double polarz = -5.6e-3;
    // double meanDepth = 575;
    // std::filesystem::path savePath("/home/simon/Documents/lab/"
    //     "/code/awg-control/Cpp/");
    // std::filesystem::path probeScanPath("/home/simon/Documents/lab/"
    //     "data/probe-det.npy");
    // std::filesystem::path testImgPath("/home/simon/Documents/lab/"
    //     "data/peak.tiff");

    // auto detProbe = npy::read_npy<double> (probeScanPath).data;
    // Eigen::Map<Eigen::ArrayXd> detProbeEigen(detProbe.data(), detProbe.size());
    // cv::Mat twzrImage = cv::imread(testImgPath, cv::IMREAD_GRAYSCALE);
    // auto peaks = ImageProcessing::getPeaks(twzrImage, -1, savePath);
    // auto twzrCrops = ImageProcessing::getSubImgs(twzrImage, peaks, savePath);
    // auto power = ImageProcessing::getPowers(twzrCrops);
    // Eigen::Map<Eigen::ArrayXd> powersEigen(power.data(), power.size());
    // auto weightedMask = getWeightedMask(
    //     detProbeEigen,
    //     powersEigen,
    //     polarz,
    //     meanDepth
    // );
    // auto powerErrs = (
    //     weightedMask * powersEigen 
    //     - (weightedMask * powersEigen).mean()
    // ) / (weightedMask * powersEigen).mean();
    // auto now{std::chrono::system_clock::now()};
    // std::chrono::time_z
    // ArrayWaveform wfm;
    // wfm.setSamplingRate(614.4e6);
    // wfm.setFreqResolution(1e3);
    // wfm.setFreqTone(105e6, 1e6, 10);
    // wfm.setDefaultParam();
    // wfm.getStaticWaveform();
    // ArrayWaveform anotherWfm = wfm;
    // auto pData = (int16_t*) wfm.getDataBuffer();
    // auto pDataC = (int16_t*) anotherWfm.getDataBuffer();
    // std::cout << pData[0] << std::endl;
    // std::cout << pDataC[0] << std::endl;
    // pData[0] = 2;
    // std::cout << pData[0] << "\n" << pDataC[0] << std::endl;

    enum class TEST_ENUMS : int32_t {
        ZERO = 0,
        ONE = 1,
        TWO = 2,
        THREE = 3
    };

    int32_t test = 0;
    switch (test) {
        case TEST_ZERO:
            std::cout << TEST_ENUMS::ZERO << std::endl;
            break;
        case TEST_ONE:
            std::cout<< "one" << std::endl;
        default:
            break;
    }

    return 0;
}