#include "../lib/waveform.h"
#include "../lib/devices/AWG.h"
#include "../lib/devices/basler.h"
#include "../lib/image_process.h"
#include "../lib/uniformization.h"

void resetAWG(AWG& awg, std::pair<void*, int64_t> wfmData) {
    // auto wfmData = wfm.getStaticWaveform();
    awg.cardStop();
    awg.writeSeqModeSegment(0, wfmData.first, wfmData.second);
    awg.writeSeqModeSegment(1, wfmData.first, wfmData.second);
    awg.setSeqModeStep(0, 0, 1, 1, AWG::SEQ_LOOPCONDITION::ONTRIG);
    awg.setSeqModeStep(1, 1, 0, 1, AWG::SEQ_LOOPCONDITION::ONTRIG);
    awg.cardRun();
    awg.forceTrigger();

}

int main() {
    // following params should ideally be loaded from
    // a .toml file
    Uniformization::Params config;
    config.probeScanPath = "";
    config.polarizability = 0;
    config.meanDepth = 550;
    config.stepSize = 50;
    config.errorThreshold = 0.003;
    config.maxLoop = 50;
    config.numImgingAvg = 10;
    config.numTweezer = 20;

    // following objects require initialization somewhere 
    // in the running program 
    AWG awg;
    // awg.open(0);
    // awg.setSampleRate(int64_t(614.4e6));
    // awg.setActiveChannels(std::set<int> {0});
    // awg.setChannel(0, 2500, AWG::CHANNEL_STOPLVL::ZERO, true);
    // awg.setTrigMode(0, AWG::TRIGGER_MODE::POS);
    // awg.setTrigMaskOr({AWG::TRIGGER_MASK::EXT0});
    // awg.initReplayModeSeq(2);
    // awg.writeSeqModeSegment(0, wfmData.first, wfmData.second);
    // awg.writeSeqModeSegment(1, wfmData.first, wfmData.second);
    // awg.setSeqModeStep(0, 0, 1, 1, AWG::SEQ_LOOPCONDITION::ONTRIG);
    // awg.setSeqModeStep(1, 1, 0, 1, AWG::SEQ_LOOPCONDITION::ONTRIG);

    BaslerCam basler;
    ArrayWaveform wfm;

    // run the uniformization routine, wfm is modified in place
    // various outputs are saved under probeScanPath
    Uniformization::run(awg, basler, wfm, config); // wfm is modified in place

    return 0;
}
